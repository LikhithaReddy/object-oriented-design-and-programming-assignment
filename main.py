# Interface Segregation Principle (ISP)
class IProduct:
    def get_name(self):
        pass

    def get_price(self):
        pass

class IOrder:
    def add_product(self, product: IProduct):
        pass

    def remove_product(self, product: IProduct):
        pass

    def calculate_total(self) -> float:
        pass

class ICustomer:
    def get_name(self):
        pass

    def get_email(self):
        pass

class IShipper:
    def ship_order(self, order: IOrder):
        pass


# Single Responsibility Principle (SRP) and Open/Closed Principle (OCP)
class Product(IProduct):
    def __init__(self, name: str, price: float):
        self._name = name
        self._price = price

    def get_name(self):
        return self._name

    def get_price(self):
        return self._price


class Order(IOrder):
    def __init__(self, customer: ICustomer, shipper: IShipper):
        self._customer = customer
        self._shipper = shipper
        self._products = []

    def add_product(self, product: IProduct):
        self._products.append(product)

    def remove_product(self, product: IProduct):
        self._products.remove(product)

    def calculate_total(self) -> float:
        return sum(product.get_price() for product in self._products)

    def place_order(self):
        # Dependency Inversion Principle (DIP)
        self._shipper.ship_order(self)


class Customer(ICustomer):
    def __init__(self, name: str, email: str):
        self._name = name
        self._email = email

    def get_name(self):
        return self._name

    def get_email(self):
        return self._email


class Shipper(IShipper):
    def __init__(self, name: str):
        self._name = name

    def ship_order(self, order: IOrder):
        print(f"Shipping order to {order._customer.get_name()} at {order._customer.get_email()}")


